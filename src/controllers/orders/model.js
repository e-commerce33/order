'use strict';

const mongoose = require('mongoose');

const mainDb = require('../../loaders/mongoose').getConnection();

const Schema = mongoose.Schema;

const orderSchema = new Schema({
  productList: {
    type: [Object],
    required: true
  },
  userEmail: {
    type: String,
    required: true
  },
  totalCost: {
    type: Number,
    min: 0,
    required: true
  },
  status: {
    type: String,
    required: true
  }
});

module.exports = mainDb.model('Order', orderSchema, 'orders');
