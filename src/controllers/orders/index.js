/* eslint-disable no-underscore-dangle */
'use strict';

const Model = require('./model'),
  apiSchema = require('./apiSchema'),
  { errorObjects } = require('../../constants/error'),
  httpStatus = require('../../constants/httpStatus'),
  log = require('../../helpers/log');

function create(req, res, next) {
  log.debug('orders.create');

  return apiSchema.create.body.validateAsync(req.body)
    .then((body) => {
      body.userEmail = req.auth.email;
      body.status = 'unconfirm';
      return Model.create(body);
    })
    .then((result) => res.status(httpStatus.CREATED).json(result))
    .catch(next);
}

function findById(req, res, next) {
  log.debug('orders.findById');

  return apiSchema.findById.params.validateAsync(req.params)
    .then(({ id }) => Model.findById(id))
    .then((order) => {
      if (order) {
        return res.status(httpStatus.OK).json(order);
      }

      throw errorObjects.NOT_FOUND;
    })
    .catch(next);
}

function getMe(req, res, next) {
  log.debug('order.getMe');
  const userEmail = req.auth.email;

  return Model.find({ userEmail })
    .then((result) => res.status(httpStatus.OK).json(result))
    .catch(next);
}

function cancel(req, res, next) {
  log.debug('order.cancel');
  const userEmail = req.auth.email;

  return apiSchema.cancel.params.validateAsync(req.params)
    .then(({ id }) => Model.findById(id))
    .then((order) => {
      if (!order) {
        throw errorObjects.NOT_FOUND;
      }
      if (order.userEmail !== userEmail) {
        throw errorObjects.FORBIDDEN;
      }

      order.status = 'canceled';

      return order.save();
    })
    .then((result) => res.status(httpStatus.OK).json(result))
    .catch(next);
}

module.exports = { create, findById, getMe, cancel };
