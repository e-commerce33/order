/* eslint-disable no-underscore-dangle */
'use strict';

const joi = require('joi');

const schema = {
  _id: joi.string().hex().length(24),
  product: joi.object().keys({
    _id: joi.string().hex().length(24).required(),
    amount: joi.number().min(0).required(),
    totalCost: joi.number().min(0).required()
  }),
  userEmail: joi.string().email(),
  totalCost: joi.number().min(0).required()
};

schema.productList = joi.array().items(schema.product).single().min(1);

module.exports = {
  create: {
    body: joi.object().keys({
      productList: schema.productList.required(),
      totalCost: schema.totalCost.required()
    })
  },
  findById: {
    params: joi.object().keys({
      id: schema._id.required()
    })
  },
  cancel: {
    params: joi.object().keys({
      id: schema._id.required()
    })
  }
};
