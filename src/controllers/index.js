'use strict';

const { combine } = require('../helpers/api'),
  orders = require('./orders');

const combined = combine({
  orders
});

module.exports = combined;
