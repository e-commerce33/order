'use strict';

const isAuthorizedAccess = require('./isAuthorizedAccess');

module.exports = { isAuthorizedAccess };
